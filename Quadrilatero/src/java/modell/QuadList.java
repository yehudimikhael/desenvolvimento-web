/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modell;

/**
 *
 * @author yehudi
 */
public class QuadList {
    
    public Double lado;

    public QuadList() {
        
    }    
        
    public void setLado(double lado){
        this.lado =  lado;
    }
    public double getLado(){
        return lado;
    }
    public double getPerimetro(){
        return lado * 4;
    }
    
    public double getArea(){
        return lado * lado;    
    }
       
    
}
