/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author yehudi
 */
public class Aluno extends Pessoa {

    private double teste, prova;
    private String Matricula;

    public Aluno() {
    }

    public double getTeste() {
        return teste;
    }

    public void setTeste(double teste) {
        this.teste = teste;
    }

    public double getProva() {
        return prova;
    }

    public void setProva(double prova) {
        this.prova = prova;
    }

    public String getMatricula() {
        return Matricula;
    }

    public void setMatricula(String Matricula) {
        this.Matricula = Matricula;
    }

    public double getMedia() {
        return (teste + prova) / 2;
    }

    public String getStatus() {
        if (getMedia() >= 7) {
            return "Aprovado";
        } else if (getMedia() < 4) {
            return "Reprovado";
        } else {
            return "Final";
        }
    }
}
