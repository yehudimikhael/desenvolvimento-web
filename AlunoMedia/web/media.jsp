<%-- 
    Document   : media
    Created on : Mar 15, 2017, 9:12:42 PM
    Author     : yehudi
--%>

<%@page import="Model.Aluno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% Aluno aluno = (Aluno) request.getAttribute("objAluno");%>
        Matricula:  <% out.println(aluno.getMatricula());%> <br/>
        Nome: <% out.println(aluno.getName());%> <br/>
        Teste: <% out.println(aluno.getTeste()); %> <br/>
        Prova: <% out.println(aluno.getProva()); %> <br/>
        Media: <% out.println(aluno.getMedia()); %> <br/>
        Status: <% out.println(aluno.getStatus()); %> <br/>
    </body>
</html>
