<%-- 
    Document   : calculosalario
    Created on : Mar 15, 2017, 9:36:30 PM
    Author     : yehudi
--%>

<%@page import="Model.Funcionario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <% Funcionario funcionario = (Funcionario) request.getAttribute("objfunc"); %>
        Nome : <% out.println(funcionario.getNome());%> <br/>
        Matricula : <% out.println(funcionario.getMatricula());%> <br/>
        Funcao : <% out.println(funcionario.getFuncao());%> <br/>
        Salario Bruto: <% out.println(funcionario.getSalarioBase());%> <br/>
        INSS : <% out.println(funcionario.getNSS());%> <br/>
        Salario Liquido : <% out.println(funcionario.getSalarioLiq());%> <br/>
    </body>
</html>
